window.onload = function() {
  // Video
  var video = document.getElementById('video')
  var currentTime = document.getElementById('current-time')
  var videoDuration = document.getElementById('video-duration')

  // Buttons
  var playButton = document.getElementById('play-pause')
  var muteButton = document.getElementById('mute')

  // Sliders
  var seekBar = document.getElementById('seek-bar')
  var volumeBar = document.getElementById('volume-bar')

  video.addEventListener('loadedmetadata', function() {
    currentTime.innerText = toTime(video.currentTime)
    videoDuration.innerText = toTime(video.duration)
  })

  // Event listener for the play/pause button
  playButton.addEventListener('click', function() {
    if (video.paused == true) {
      playVideo()
    } else {
      pauseVideo()
    }
  })

  // Event listener for the mute button
  muteButton.addEventListener('click', function() {
    if (video.muted == false) {
      // Mute the video
      video.muted = true
      // Update the button text
      muteButton.innerHTML = 'Unmute'
    } else {
      // Unmute the video
      video.muted = false
      // Update the button text
      muteButton.innerHTML = 'Mute'
    }
  })

  // Event listener for the seek bar
  seekBar.addEventListener('change', function() {
    // Calculate the new time
    var time = video.duration * (seekBar.value / 100)
    // Update the video time
    video.currentTime = time
  })

  // Pause the video when the slider handle is being dragged
  seekBar.addEventListener('mousedown', pauseVideo)

  // Play the video when the slider handle is dropped
  seekBar.addEventListener('mouseup', playVideo)

  // Update the seekbar on play
  video.addEventListener('timeupdate', function(event) {
    seekBar.value = (event.target.currentTime / event.target.duration) * 100
    currentTime.innerText = toTime(video.currentTime)
  })

  // Event listener for the volume bar
  volumeBar.addEventListener('change', function() {
    // Update the video volume
    video.volume = volumeBar.value
  })

  function playVideo() {
    // play
    video.play()
    // Update the button text to 'Pause'
    playButton.innerHTML = 'Pause'
  }

  function pauseVideo() {
    // pause
    video.pause()
    // Update the button text to 'Play'
    playButton.innerHTML = 'Play'
  }

  function toTime(timeToParse) {
    var timeInSeconds = parseInt(timeToParse, 10)
    var hours = Math.floor(timeInSeconds / 3600)
    var minutes = Math.floor((timeInSeconds - hours * 3600) / 60)
    var seconds = timeInSeconds - hours * 3600 - minutes * 60

    if (hours < 10 && hours > 0) hours = '0' + hours
    if (minutes < 10) minutes = '0' + minutes
    if (seconds < 10) seconds = '0' + seconds

    var time = hours ? hours + ':' : ''
    time += minutes + ':'
    time += seconds

    return time
  }
}
